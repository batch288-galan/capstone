import React from 'react';
import { Nav, Navbar, NavDropdown,  Container } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

const AdminNavbar = () => {
    return (
        <Navbar position="fixed" bg="dark" variant="dark">
           <Container>
             {/*<Navbar.Brand href="#home">ADMIN</Navbar.Brand>*/}
             <Nav variant="pills" className="m-auto ">
               <Nav.Link className="adminNavLink" as = {NavLink} to = "/admin/dashboard">Dashboard</Nav.Link>
               {/*<NavDropdown title="Products" id="nav-dropdown">
                       <NavDropdown.Item as={NavLink} to="/admin/products">All Products</NavDropdown.Item>
                       <NavDropdown.Item as={NavLink} to="/admin/add-product">Add Products</NavDropdown.Item>
                </NavDropdown>*/}
               <Nav.Link className="adminNavLink" as = {NavLink} to = "/admin/products">Products</Nav.Link>
               <Nav.Link className="adminNavLink" as = {NavLink} to = "/admin/add-product">Add Products</Nav.Link>
               <Nav.Link className="adminNavLink" as = {NavLink} to = "/admin/orders">Orders</Nav.Link>
               <Nav.Link className="adminNavLink" as = {NavLink} to = "/admin/users">Users</Nav.Link>
             </Nav>
           </Container>
        </Navbar>
    ); 
}

export default AdminNavbar;
