import React from 'react';
import {useEffect, useState} from "react";
import UsersData from "../admin/UsersData.js";
import { Table, Container, Button } from 'react-bootstrap';
// import { MdOutlineAdd } from "react-icons/md";
// import AddProductModal from '../admin/AddProduct.js';

const API = `${process.env.REACT_APP_API_URL}/user/users-list`;

const UsersTable = () => {
    const [users, setUsers] = useState([]);

    const fetchUsers = (token) => {
      fetch(API, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then(data=> {

            console.log(data)
            if (data.length > 0) {
                setUsers(data);
            }

      })
    }

    useEffect(() => {
        fetchUsers(API);
    }, [])



    return (

        

            <>
                <h3 className="text-center mt-4">LIST OF REGISTERED USERS</h3>
                <Container fluid className = "mt-4">
              
                <Table striped bordered hover>
                  <thead>
                    {/*<tr>
                        <th colSpan = '4'>Products</th>
                    </tr>*/}
                  </thead>

                  <thead>
                    <tr className="table-header table-primary">
                        <th>#</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th className="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <UsersData users={users}/>
                  </tbody>
                </Table>
                </Container>
            </>

        )
    
}

export default UsersTable;