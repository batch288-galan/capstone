import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import OrdersData from '../admin/OrdersData';

const API = `${process.env.REACT_APP_API_URL}/orders/all-orders`;

const OrdersTable = () => {
  const [orders, setOrders] = useState([]);

  const fetchOrders = async (token) => {
    try {
      const response = await fetch(API, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
      const data = await response.json();
      setOrders(data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchOrders(API);
  }, []);

  return (
    <>
    <h3 className="text-center mt-4"> LIST OF ORDERS </h3>
    <Container fluid className="mt-3">
      <OrdersData orders={orders} />
    </Container>
    </>
  );
};

export default OrdersTable;