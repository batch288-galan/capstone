import { Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';
 
export default function AdminDashboardCard() {

const [activeProducts, setActiveProdcuts] = useState('');
const [publicUsers,setPublicUsers] = useState('');

useEffect(() => {

		fetch('http://localhost:3005/products/products-count')
		.then(result => result.json())
		.then(data => {
			setActiveProdcuts(data)   
		})

}, [])

useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/user/count-users`)
		.then(result => result.json())
		.then(data => {
			setPublicUsers(data)   
			console.log(data)
		})

}, [])



	return(

			<>
				<Container className = "mt-5">
					<Row>
						<Col className = " col-md-6">
						    <div className="card">
						        <div className="card-body">
						            <div className="d-flex">
						                <div className="flex-grow-1">
						                    <p className="text-truncate font-size-14 mb-2">All Products</p>
						                    <h4 className="mb-2">{activeProducts}</h4>
						                    {/*<p className="text-muted mb-0"><span className="text-success fw-bold font-size-12 me-2"><i className="ri-arrow-right-up-line me-1 align-middle"></i>9.23%</span>from previous period</p>*/}
						                </div>
						                <div className="avatar-sm">
						                    <span className="avatar-title bg-light text-primary rounded-3">
						                        <i className="ri-shopping-cart-2-line font-size-24"></i>  
						                    </span>
						                </div>
						            </div>                                            
						        </div>
						    </div>
						</Col>

						<Col className = " col-md-6">
						    <div className="card">
						        <div className="card-body">
						            <div className="d-flex">
						                <div className="flex-grow-1">
						                    <p className="text-truncate font-size-14 mb-2">Users</p>
						                    <h4 className="mb-2">{publicUsers}</h4>
						                    {/*<p className="text-muted mb-0"><span className="text-success fw-bold font-size-12 me-2"><i className="ri-arrow-right-up-line me-1 align-middle"></i>9.23%</span>from previous period</p>*/}
						                </div>
						                <div className="avatar-sm">
						                    <span className="avatar-title bg-light text-primary rounded-3">
						                        <i className="ri-shopping-cart-2-line font-size-24"></i>  
						                    </span>
						                </div>
						            </div>                                            
						        </div>
						    </div>
						</Col>
						


					</Row>
				</Container>
			</>

		)
}