import React, { useEffect, useState } from 'react';
import { Table, Container, Button } from 'react-bootstrap';
import { MdOutlineAdd } from 'react-icons/md';
import AddProduct from '../admin/AddProduct';
import ProductsData from '../admin/ProductsData';
import { Link } from 'react-router-dom';

const API = `${process.env.REACT_APP_API_URL}/products/all-products`;

const ProductsTable = () => {
const [products, setProducts] = useState([]);

  const fetchProducts = () => {
    fetch(API, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(result => result.json())
      .then(data => {
        console.log(data);
        if (data.length > 0) {
          setProducts(data);
        }
      });
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  

  return (
    <>  
    
      <h3 className="text-center mt-4">LIST OF PRODUCTS</h3>
      <Container fluid className="mt-3">         
       
        <Table striped bordered hover>
          <thead>
            <tr className="table-header table-primary">
              <th>#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Status</th>
              <th className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <ProductsData products={products} />
          </tbody>
        </Table>
      </Container>
    </>
  );
};

export default ProductsTable;