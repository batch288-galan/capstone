
import { Container, Col, Row, Card } from 'react-bootstrap';

export default function AboutUs() {
	return(

			<Container >
			    <Row className="aboutUsContainer text-center">
			        <Col className="col-12 mt-3">
			            <Card className="aboutUsCard">
			                <Card.Body>
			                    <Card.Title className="mb-3 cardTitle">ABOUT US</Card.Title>
			                    <Card.Text className="cartText">We are a guitar store that is passionate about music. We believe that everyone should have the opportunity to learn and play the guitar, and we are here to help you find the right guitar for your needs.
			                    </Card.Text>
			               </Card.Body>
			            </Card>
			        </Col>
			    </Row> 
			</Container>

		)
}