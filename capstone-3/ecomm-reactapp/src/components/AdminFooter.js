import React from 'react';

const AdminFooter = () => {
  return (
    <footer className="admin-footer">
          &copy; 2023 Galan. All rights reserved.
    </footer>
  );
};

export default AdminFooter;
 