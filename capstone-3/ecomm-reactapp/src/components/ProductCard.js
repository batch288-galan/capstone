import {Card, Button, Container, Row, Col} from 'react-bootstrap';

export default function ProductCard(props){

const { id, name, description, price } = props.propProducts;

	return(

				<Container>
					<Row>
						<Col>
							<Card className = "mt-3" >
						      <Card.Body>
						        <Card.Title> {name} </Card.Title>
						        <Card.Subtitle> Description </Card.Subtitle>
						        <Card.Text> {description} </Card.Text>
						        <Card.Subtitle> Price </Card.Subtitle>
						        <Card.Text> {price} </Card.Text>
						        <Button variant="primary">Details</Button>
						      </Card.Body>
							</Card>
						</Col>
					</Row>
				</Container>

		  )
}