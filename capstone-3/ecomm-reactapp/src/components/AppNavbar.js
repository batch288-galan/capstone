import {Container, Navbar, Nav, Form, Button} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import UserContext from '../UserContext.js';
import {useContext, useState, useEffect} from 'react';
import { AiOutlineShoppingCart } from "react-icons/ai";

export default function AppNavbar(){

const {user} = useContext(UserContext);
const [email, setEmail] = useState('');

useEffect(() => {
	fetch(`${process.env.REACT_APP_API_URL}/user/user-details`, {
	  method: 'GET',
	  headers: {
	    Authorization: `Bearer ${localStorage.getItem('token')}`
	  }
	})
	.then(result => result.json())
	.then(data => {
	   	setEmail(data.email)
	})
})

	return(

			<>

		    <Navbar expand="lg" className="appNav bg-body-tertiary">
		      <Container >
		      {/*<Navbar.Brand href="#home">
		                  <img
		                    src="./images/logo.jpg"
		                    width="41"
		                    height="41"
		                    className="d-inline-block align-top rounded-circle"
		                    alt="logo"
		                  />
		                </Navbar.Brand>*/}
		        <Navbar.Brand as={NavLink} to='/' >ES Guitar Store</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		          
		            <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
		            <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
		            

		            {
		            	user.id !== null 
		            	?
		            	<>
		            	<Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
		            	<Nav.Link as = {NavLink} to = '/logout' > {email} </Nav.Link>
		            	</>
		            	:
		            	<>
		            	{/*<Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>*/}
		            	<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
		            	</>
		            	
		            }

		            <Nav.Link><AiOutlineShoppingCart size={24} /></Nav.Link>
		          </Nav>
		        </Navbar.Collapse>
		        
		      </Container>
		    </Navbar>

		    {/*<Form className="d-flex form-search">
              <Form.Control
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
              />
              <Button variant="outline-success">Search</Button>
            </Form>*/}

		    		            </>
		  )
}