import React from 'react';

function Cart({ items, total }) {
  return (
    <div className="cart">
      <h2>Cart</h2>
      {items.length === 0 ? (
        <p>Your cart is empty.</p>
      ) : (
        <>
          <ul className="cart-items">
            {items.map((item) => (
              <li key={item.id}>
                <span>{item.name}</span>
                <span>{item.quantity} x ${item.price}</span>
              </li>
            ))}
          </ul>
          <p>Total: ${total}</p>
          <button>Checkout</button>
        </>
      )}
    </div>
  );
}

export default Cart;