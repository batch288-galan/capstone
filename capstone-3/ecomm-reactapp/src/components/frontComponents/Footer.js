import React from 'react';

const Footer = () => {
  return (
    <footer fixed="bottom" className="footer">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <h4>About Us</h4>
            <p>We are a guitar store that is passionate about music. We believe that everyone should have the opportunity to learn and play the guitar, and we are here to help you find the right guitar for your needs.</p>
          </div>
          <div className="col-md-4">
                      <h4>Contact Us</h4>
                      <p>123 Main Street
                      Anytown, CA 12345
                      (123) 456-7890</p>
                      <p><a href="mailto:info@guitarstore.com">info@guitarstore.com</a></p>
                    </div>
          <div className="col-md-4">
            <h4>Follow Us</h4>
            <ul className="list-unstyled">
              <li><a href="https://www.facebook.com/guitarstore">Facebook</a></li>
              <li><a href="https://twitter.com/guitarstore">Twitter</a></li>
              <li><a href="https://www.instagram.com/guitarstore">Instagram</a></li>
            </ul>
          </div>
        </div>
        <div className="copyright">
          &copy; 2023 Guitar Store. All rights reserved.
        </div>
      </div>
    </footer>
  );
};

export default Footer;
