import './App.css';
import { UserProvider } from './UserContext.js';
import { useState, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
// react bootstrap

import 'bootstrap/dist/css/bootstrap.min.css';

/*admin*/
import AdminHome from './admin/Home.jsx';
import Products from './admin/Products.js';
import Orders from './admin/Orders.js';
import Users from './admin/Users.js';
import AddProduct from './admin/AddProduct.js';
import Checkout from './frontend/Checkout.js'

// pages importation
import Home from './frontend/Home.js';
// import Products from './pages/Products.js';
import Login from './auth/Login.js';
import Register from './auth/Register.js';
import Logout from './auth/Logout.js';
import ViewProductDetails from './frontend/ViewProductDetails';
import EditProduct from './admin/EditProduct.js';
import Archive from './admin/Archive.js';


/*frontend*/
import PageNotFound from './frontend/PageNotFound.js';
import ProductsCatalog from './frontend/ProductsCatalog';

//Routing
import {BrowserRouter, Route, Routes} from 'react-router-dom';


function App() {

const [user, setUser] = useState({
  id: null,
  isAdmin: null
})

const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {

    if(localStorage.getItem('token')) {
        fetch(`${process.env.REACT_APP_API_URL}/user/user-details`, {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(result => result.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

}, [])

const Inaccessible = () => {
   return <Navigate to="/inaccessible" />;
 }

 const isAdmin = (user) => {
   return user.isAdmin === true;
 };

  return (
  <UserProvider value = {{ user, setUser, unsetUser }}>
    <BrowserRouter>

        <Routes>       
            <Route path = '/' element = {<Home/>} />
            <Route path = '/logout' element = {<Logout />} />
            <Route path = '/products' element = {<ProductsCatalog />} />
            <Route path="/products/:productId" element={<ViewProductDetails />} />
            <Route path="/admin/products/:productId" element={<EditProduct />} />
            <Route path="/admin/products/:productId/archive" element={<Archive />} />
            <Route path="/products/:productId/checkout" element={<Checkout />} />

            {user.id === null ? (
              <>
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
              </>
            ) : (
              <>
                <Route path="/register" element={<Inaccessible />} />
                <Route path="/login" element={<Inaccessible />} />
              </>
            )}

              <Route path = '/admin/dashboard' element = {<AdminHome />} />
              <Route path = '/admin/products' element = {<Products />} />
              <Route path = '/admin/orders' element = {<Orders />} />
              <Route path = '/admin/users' element = {<Users />} />
              <Route path = '/admin/add-product' element = {<AddProduct />} />
              

            

            <Route path="/inaccessible" element={<PageNotFound/>} />
            <Route path="*" element={<Inaccessible />} />

        </Routes>
       
        
    </BrowserRouter>
  </UserProvider>
  );
}

export default App;
