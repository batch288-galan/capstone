const products = [
    {
      "id": "001",
      "name": "Laptop",
      "description": "15-inch MacBook Pro",
      "price": 1799.00,
      "isActive": true
    },
    {
      "id": "002",
      "name": "Smartphone",
      "description": "iPhone 12 Pro Max",
      "price": 1099.00,
      "isActive": true
    },
    {
      "id": "003",
      "name": "Smartwatch",
      "description": "Apple Watch Series 6",
      "price": 399.00,
      "isActive": true
    },
    {
      "id": "004",
      "name": "Gaming Console",
      "description": "Xbox Series X",
      "price": 499.00,
      "isActive": true
    },
    {
      "id": "005",
      "name": "Wireless Earbuds",
      "description": "AirPods Pro",
      "price": 249.00,
      "isActive": true
    }
]

export default products;