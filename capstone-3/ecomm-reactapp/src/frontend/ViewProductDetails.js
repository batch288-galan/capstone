import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal2 from 'sweetalert2';
import AppNavbar from '../components/AppNavbar';

const API = `${process.env.REACT_APP_API_URL}/orders/add-cart`;

export default function ViewProductDetails() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [quantity, setQuantity] = useState(1);
  const { productId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    fetch(`http://localhost:3005/products/${productId}`)
      .then(result => result.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      })
      .catch(error => {
        console.error(error);
      });
  }, [productId]);

  const handleCheckout = () => {
    fetch(API, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity
      })
    })
      .then(result => result.json())
      .then(data => {
        if (data) {
          Swal2.fire({
            title: 'Successfully placed order',
            icon: 'success'
          });
          navigate('/products');
        } else {
          Swal2.fire({
            title: 'Error, try again',
            icon: 'error'
          });
        }
      })
      .catch(error => {
        console.error(error);
        Swal2.fire({
          title: 'Error, try again',
          icon: 'error'
        });
      });
  };

  return (
    <>
      <AppNavbar />
      <Container className="mt-5">
        <Row>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Form>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control style={{ width: '5%' }} value={quantity} onChange={event => setQuantity(event.target.value)} type="number" />
                  </Form.Group>
                </Form>
                <Button className="me-3" onClick={handleCheckout} variant="dark">Buy Now</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}