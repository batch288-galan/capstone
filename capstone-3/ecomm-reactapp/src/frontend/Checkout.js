import { useState, useEffect } from 'react';
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap';
import { Link, useLocation, useNavigate } from 'react-router-dom';

import AppNavbar from '../components/AppNavbar';

const API = `${process.env.REACT_APP_API_URL}/orders/checkout`;

export default function Checkout() {

  const navigate = useNavigate()

  const [quantity, setQuantity] = useState(1);
  const { state: { product } } = useLocation();
  const [orderPlaced, setOrderPlaced] = useState(false);

  const { name, description, price, _id: productId } = product;

  const handleBuyNow = () => {
    const token = localStorage.getItem('token');

    fetch(API, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        orders: [{
          productId,
          quantity
        }]
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          setOrderPlaced(true);
        } else {
          alert('Failed to place order');
        }
      })
      .catch(error => console.error(error));
  };

  const handleAddToCart = () => {
    // Add product to cart
  };

  useEffect(() => {
    if (orderPlaced) {
      setTimeout(() => {
        navigate('/orders');
      }, 3000);
    }
  }, [orderPlaced]);

  return (
    <>
      <AppNavbar />
      <Container className="mt-5">
        <Row>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Form>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control style={{ width: '5%' }} value={quantity} onChange={(event) => setQuantity(event.target.value)} type="number" />
                  </Form.Group>
                </Form>
               
                <Button onClick={handleBuyNow} variant="secondary">Place Order</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}