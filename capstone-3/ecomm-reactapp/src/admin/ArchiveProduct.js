import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const ArchiveProductModal = ({ show, onHide, product, onArchive }) => {
  const { name, isActive } = product;

  const handleArchiveClick = () => {
    onArchive(product._id, isActive);
  };
 
  return (
    <Modal show={show} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title>{isActive ? 'Archive' : 'Activate'} Product</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>{`Are you sure you want to ${isActive ? 'archive' : 'activate'} ${name}?`}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={onHide}
          disabled={!isActive}
        >
          Cancel
        </Button>
        <Button
          variant="primary"
          onClick={handleArchiveClick}
        >
          {isActive ? 'Archive' : 'Activate'}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};


export default ArchiveProductModal;