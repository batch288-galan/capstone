import AdminHeader from '../components/AdminHeader.js';
import AdminNavbar from '../components/AdminNavbar.js';
import ProductsTable from '../components/ProductsTable.js';

export default function Products() {
	return(

			<> 
				<AdminHeader />
				<AdminNavbar />
				<ProductsTable />
			</>
		)
}