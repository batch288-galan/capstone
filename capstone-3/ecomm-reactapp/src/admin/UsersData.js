import { Button } from 'react-bootstrap';
 
const UsersData = ({users}) => {
    let i=0;
    return (
        <>
            {
                users.map((user) => {
                    i++;
                    const {_id, userName, email} = user;

                    return (
                        <tr className="table-data" key={_id}>
                            <td>{i}</td>
                            <td>{userName}</td>
                            <td>{email}</td>
                            {/*<td className="text-center">{ isActive? "Active" : "Inactive" }</td>*/}
                            <td className="text-center"><Button className="btn-sm" variant="dark" > Edit </Button> | <Button variant="secondary" className="btn-sm" > Archive </Button></td>
                        </tr>
                    )
                })
   
            }
        </>
    )
}

export default UsersData;