import React, { useContext, useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import AdminHeader from '../components/AdminHeader.js';
import AdminNavbar from '../components/AdminNavbar.js';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';
const Archive = () => {
  const { productId } = useParams();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const { setUser, User } = useContext(UserContext);
  const [isArchived, setIsArchived] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    fetch(`c/products/${productId}`) 
      .then(result => result.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setIsArchived(isArchived);
      })
      .catch(error => console.error(error));
  }, [productId]);

  const handleArchive = (event) => {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/${productId}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isActive: !isArchived
      })
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);

        if (data === true) {
            Swal2.fire({
              title: `Product Details Successfully ${isArchived? "archived" : "activated"}`,
              icon: "success"
            }) 

            navigate('/admin/products')

        }  else {
              alert("Error!")
            }
      // Redirect to the product details page after editing the product
    })
    .catch(error => console.error(error));
  };

  return (
    <>
      <AdminHeader />
      <AdminNavbar />
      <div className="container mt-5">
        <h1>Choose ...</h1>
        <Form onSubmit={handleArchive}>

              <Form.Group controlId="isArchived">
                <Form.Check
                  type="radio"
                  name="archive"
                  label="Archive"
                  value={true}
                  checked={isArchived}
                  onChange={(event) => setIsArchived(event.target.value === 'true')}
                />
                <Form.Check
                  type="radio"
                  name="archive"
                  label="Activate"
                  value={false}
                  checked={!isArchived}
                  onChange={(event) => setIsArchived(event.target.value === 'true')}
                />
              </Form.Group>

          <div className="mt-5">
            <Button variant="primary" type="submit">
              {isArchived ? 'Archive' : 'Activate'}
            </Button>
          </div>
        </Form>
      </div>
    </>
  );
};

export default Archive;