import { useState } from 'react';
import { Container, Form, Button, Row, Col } from 'react-bootstrap';
import AdminNavbar from '../components/AdminNavbar';
import AdminHeader from '../components/AdminHeader';
import { useNavigate } from 'react-router-dom';
import Swal2 from 'sweetalert2';

const API = `${process.env.REACT_APP_API_URL}/products/add-product`;

export default function AddProduct() {
  const navigate = useNavigate();
  const [products, setProducts] = useState([{ name: '', description: '', price: '', category: '' }]);

  const handleAddForm = () => {
    setProducts([...products, { name: '', description: '', price: '', category: '' }]);
  };

  const handleInputChange = (event, index) => {
    const { name, value } = event.target;
    const list = [...products];
    list[index][name] = value;
    setProducts(list);
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    fetch(API, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(products)
    })
      .then(result => result.json())
      .then(data => {
        if (data === true) {
          Swal2.fire({
            title: 'Successfully Added!',
            icon: 'success'
          });

          navigate('/admin/products');
        } else {
          alert('Error adding products!');
        }
      });
  };

  const handleRemoveForm = () => {
    const list = [...products];
    list.pop();
    setProducts(list);
  };

  return (
    <>
      <AdminHeader />
      <AdminNavbar />

      <Button className="btnAddForm" variant="primary" onClick={handleAddForm}>Add</Button>
      <Button className="btnRemoveForm" variant="secondary" onClick={handleRemoveForm}>Remove</Button>

      <h3 className="text-center mt-4">ADD PRODUCTS</h3>
      <Container className="mt-3 add-product-conatainer">
        <Form onSubmit={handleFormSubmit}>
          {products.map((product, index) => (
            <Row key={index}>
              <Col className="col-md-3">
                <Form.Group className="mb-3" controlId={`productName-${index}`}>
                  <Form.Label>Product Name</Form.Label>
                  <Form.Control type="text" name="name" value={product.name} onChange={(event) => handleInputChange(event, index)} />
                </Form.Group>
              </Col>
              <Col className="col-md-5">
                <Form.Group className="mb-3" controlId={`description-${index}`}>
                  <Form.Label>Description</Form.Label>
                  <Form.Control as="textarea" rows={1} name="description" value={product.description} onChange={(event) => handleInputChange(event, index)} />
                </Form.Group>
              </Col>
              <Col className="col-md-2">
                <Form.Group className="mb-3" controlId={`category-${index}`}>
                  <Form.Label>Category</Form.Label>
                  <Form.Select name="category" value={product.category} onChange={(event) => handleInputChange(event, index)}>
                    <option value="">Select...</option>
                    <option value="Electric">Electric</option>
                    <option value="Acoustic">Acoustic</option>
                    <option value="Bass">Bass</option>
                    <option value="Accessories">Accessories</option>
                    <option value="Picks">Picks</option>
                    <option value="Cases">Cases</option>
                  </Form.Select>
                </Form.Group>
              </Col>
              <Col className="col-md-1">
                <Form.Group className="mb-3" controlId={`price-${index}`}>
                  <Form.Label>Price</Form.Label>
                  <Form.Control type="text" name="price" value={product.price} onChange={(event) => handleInputChange(event, index)} />
                </Form.Group>
              </Col>
            </Row>
          ))}
          <div className="text-center mb-3">
            <Button variant="primary" type="submit">Add </Button>
          </div>
        </Form>
      </Container>
    </>
  );
}