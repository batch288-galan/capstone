import React, { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const API = `${process.env.REACT_APP_API_URL}/products/:productId/archive`;

const ProductsData = ({ products, onEdit, onArchive }) => {
  let i = 0;

  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const handleClose = () => setShowModal(false);

  return (
    <>
      {products.map(product => {
        i++;
        const { _id, name, description, price, isActive } = product;

        return (
          <tr className="table-data" key={_id}>
            <td>{i}</td>
            <td>{name}</td>
            <td>{description}</td>
            <td>₱ {price}</td>
            <td className="text-center">{isActive ? 'Active' : 'Inactive'}</td>
            <td className="text-center">
              <Button className="btn-sm me-2" variant="secondary" as={Link} to={`/admin/products/${_id}/archive`}>{isActive ? 'Archive' : 'Activate'}</Button> 
              <Button className="btn-sm me-2" variant="dark" onClick={() => {
                setSelectedProduct(product);
                setShowModal(true);
              }}>
                Details
              </Button>
              <Button className="btn-sm" as={Link} to={`/admin/products/${_id}`}>Edit</Button> 
            </td>
          </tr>
        );
      })}
      {selectedProduct && (
        <Modal show={showModal} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{selectedProduct.name}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>{selectedProduct.description}</p>
            <p>Price: ₱ {selectedProduct.price}</p>
            <p>{selectedProduct.isActive ? 'Active' : 'Inactive'}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </>
  );
}; 

export default ProductsData;