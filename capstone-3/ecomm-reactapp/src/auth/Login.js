import AppNavbar from '../components/AppNavbar.js';
import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Footer from '../components/frontComponents/Footer';

import Swal2 from 'sweetalert2';

import UserContext from '../UserContext.js';

export default function Login(){

  const [email, setEmail] = useState('');
  const [password, setPassword] =  useState('');
  const [isDisabled, setIsDisabled] = useState(true);

  const {setUser} = useContext(UserContext);

  const navigate = useNavigate();

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/user/user-details`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(result => result.json())
    .then(data=> {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })

      if (data.isAdmin) {
          navigate('/admin/dashboard');
      } else {
        navigate('/');
      }

    })
  }

  useEffect(() => {

    if (email !== '' && password !== '') {
      setIsDisabled(false);
    } else {
      setIsDisabled(true)
    }

  }, [email, password]) //dependencies

  const Login = (e) => {

    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: 'POST',
      headers: {
        'Content-type' : 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(result => result.json())
    .then(data => {
      if (data === false) {
        
        Swal2.fire({
          title: 'Athentication Failed!',
          icon: 'error',
          text: 'Check you login details and try again!'
        })

      } else {
        
        localStorage.setItem('token', data.auth);
        retrieveUserDetails(data.auth);

        Swal2.fire({
          title: 'Login Successful!',
          icon: 'success'
        })

        
      }
    })

  }

  return(
      <>
      <Container fluid className="loginPage">
      <AppNavbar />
      
      <Container className = "mt-5 loginContainer">
        <Row className="loginRow">
        <Col>
        </Col>
          <Col className = "col-6">

          <div className="formLogin float-end">
              <h1 className = "text-center">Login</h1>
              <Form  onSubmit = {event => Login(event)}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Email address</Form.Label>
                      <Form.Control value = {email} onChange = {event => setEmail(event.target.value)} type="email" placeholder="Enter email" />
                      
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Password</Form.Label>
                      <Form.Control value = {password} onChange = {event => setPassword(event.target.value)} type="password" placeholder="Password" />
                    </Form.Group>

                    <p>No Account Yet? <Link as = {Link} to = '/register'>Sign-Up Here</Link></p>
                    <div className="text-center">
                      <Button className="loginBtn" disabled = {isDisabled} variant="dark" type="submit">
                        Login
                      </Button>
                    </div>
                </Form>
          </div>

            
          </Col>

        </Row>
      </Container> 
      </Container>
      <Footer />
      </>
    )
}