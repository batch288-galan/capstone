const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : true
	},

	email : {
		type : String
	},

	orderId : {
		type : String,
	default : ''
	},

	status : {
		type: String,
	default : "pending"
	},

	shippingAddress: String,

	products : [

			{
				productId : {
					type : String,
					required : true
				},

				quantity : {
					type: Number,
					default: 1
				},
				
				price : Number,
				productName : String
			}

		],

	totalAmount : Number,
	purchasedOn : {
		type : Date,
		default : Date.now
	}	

})


const Order = mongoose.model('Order', orderSchema);

module.exports = Order;