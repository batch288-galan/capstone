const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : true
	},

	description : {
		type : String,
		required : true
	},

	price : {
		type : Number,
		required : true
	},

	category: {
		type : String
	},

	isActive : {
		type : Boolean,
		default : true
	},

	createdOn : {
		type : Date,
		default : new Date()
	},
	
	createdBy : String
})

const Product = mongoose.model('Product', productSchema);

module.exports = Product;