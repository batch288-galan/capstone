const express = require('express');
const shoppingCartControllers = require('../controllers/shoppingCartControllers.js');
const auth = require('../auth.js')

const router = express.Router();

/*Adding products to cart*/
router.post('/add-to-cart', auth.verify, shoppingCartControllers.addToCart);
router.get('/view-items', auth.verify, shoppingCartControllers.viewItemsFromCart);

module.exports = router;