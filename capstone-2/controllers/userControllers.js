const User = require('../models/Users.js');
const Order = require('../models/Orders.js');
const Product = require('../models/Products.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

/*User Registration*/

module.exports.registerUser = (request, response) => {
	// console.log('Register function called');
	// console.log('request body:', request.body);
	User.findOne({ email : request.body.email })
	.then(result => {
		if(result == null) {
			let newUser = new User({
				userName : request.body.userName,
				email : request.body.email,
				password : bcrypt.hashSync(request.body.password, 10)
			})
			newUser.save();
			return response.send(true)
		} else {
			return response.send(false);
		}
	})
	.catch(err => response.send(false))
}

/*User Authentication*/

module.exports.userLogin = (request, response) => {
	// console.log('Login function called');
	// console.log('request body:', request.body);
	User.findOne({ email : request.body.email })
	.then(result => {
	  // console.log('result:', result);
	  if(!result) {
	    return response.send(false)
	  } else {
	    const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
	    // console.log('isPasswordCorrect:', isPasswordCorrect);

	    if(isPasswordCorrect){  
	      return response.send({auth: auth.createAccessToken(result)})
	    } else {
	      return response.send(false);
	    }
	  }
	})
	.catch(error => {
	  console.log('error:', error);
	  response.send(false);
	});
}


/*Retrieve User Details*/ 

module.exports.getProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	User.findOne({ _id : userData.id })
	.then(result => {
		result.password = 'Encrypted!';
		return response.send(result)
	})
	.catch(err => response.send(false))

	/*if (userData.isAdmin) {
		User.findById(userId)
		.then(result => {
			result.password = `Encrypted`;
			return response.send(result);
		})
		.catch(err => response.send(err))
	} else {
		return response.send(`You don't have access to this route!`)
	}*/

}

/*Set User As Admin*/

module.exports.setUser = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params.userId;
	let newAdmin = { isAdmin : request.body.isAdmin };

	if (userData.isAdmin) {
		User.findByIdAndUpdate(userId, newAdmin)
		.then(result => response.send(true))
		.catch(err => response.send(err));
	} else {
		return response.send(false)
	}
}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	User.findOne({_id : userData.id })
	.then(data => response.send(data));
}

module.exports.countUsers = (request, response) => {
 
 	User.find({ isAdmin: false })
 	  .then(result => {
 	    response.status(200).json(result.length);
 	  })
 	  .catch(err => {
 	    console.error(err);
 	    response.status(500).json({ error: "Failed to count active users!" });
 	  });

  
};


module.exports.getUsersList = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		User.find({ isAdmin : false })
		.then(result => response.send(result))
		.catch(err => response.send(false))
	} else {
		return response.send(false)
	}

	
}