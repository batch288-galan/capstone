const User = require('../models/Users.js');
const Product = require('../models/Products.js');
const auth = require('../auth.js');

/*module.exports.addToCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		return response.send(`Admin cannot create order!`);
	} else {
		User.findOne({ _id: userData.id })
		.then(result => {
			if (result != null) {

				const product = Product.findById(order.productId);

				result.shoppingCart.push({
					userName: product.name,
					userId: product._id,
					products: [{
						productId: 
						productName:
						quantity:
						subTotal:
					}],

					totalAmount: totalAmount;
				})
			}
		})
	}
	
}*/


module.exports.addToCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		return response.send(`Admin cannot create order!`);
	} else {
		try {
			const user = await User.findById(userData.id); // find user by ID
			const itemToAdd = request.body.itemToAdd;

			const product = await Product.findById(itemToAdd.productId); // find product by ID
			const subTotal = itemToAdd.quantity * product.price; // calculate sub-total
			const totalAmount = user.shoppingCart.reduce((total, item) => total + item.totalAmount, subTotal); // calculate total amount

			const newCartItem = {
				userName: user.userName,
				userId: user._id,
				products: [{
					productId: product._id,
					productName: product.name,
					quantity: itemToAdd.quantity,
					subTotal: subTotal
				}],
				totalAmount: totalAmount
			};

			user.shoppingCart.push(newCartItem); // add new cart item to user's shopping cart
			await user.save(); // save user to database

			response.send(true)
		} catch (err) {
			response.send(false)
		}
	}
}

module.exports.viewItemsFromCart = async (request, response) => {
 
  const userData = auth.decode(request.headers.authorization);
  const user = await User.findById(userData.id);
  const cartItems = user.shoppingCart;

  if (cartItems.length == 0) {
  	return response.send(`Your cart is empty!`)
  } else {
  	cartItems.forEach(item => response.send(item));
  }  
}

